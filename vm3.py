from PyQt5 import QtCore, QtGui, QtWidgets
import numpy as np
import math
import matplotlib.pyplot as plt
import time
from openpyxl import Workbook
class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 300)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")

    
        # button 
        self.nextstep = QtWidgets.QPushButton(self.centralwidget)
        self.nextstep.setGeometry(QtCore.QRect(70, 230, 90, 23))
        self.nextstep.setObjectName("nextstep")
        self.nextstep.clicked.connect(self.click_button)


        self.xs = QtWidgets.QLineEdit(self.centralwidget)
        self.xs.setGeometry(QtCore.QRect(40, 90, 133, 20))
        self.xs.setObjectName("kolvo_tochek")
        self.xs.setValidator(QtGui.QRegExpValidator(QtCore.QRegExp("[-]{0,1}\d{0,}\.\d{0,}")))
        self.xs_text = QtWidgets.QLabel(self.centralwidget)
        self.xs_text.setGeometry(QtCore.QRect(40, 60, 131, 21))
        self.xs_text.setObjectName("xs")
        self.T = QtWidgets.QLineEdit(self.centralwidget)
        self.T.setGeometry(QtCore.QRect(40, 150, 133, 20))
        self.T.setObjectName("kolvo_tochek")
        self.T.setValidator(QtGui.QRegExpValidator(QtCore.QRegExp("[-]{0,1}\d{0,}\.\d{0,}")))
        self.T_text = QtWidgets.QLabel(self.centralwidget)
        self.T_text.setGeometry(QtCore.QRect(40, 120, 131, 21))
        self.T_text.setObjectName("T")
        self.t_user = QtWidgets.QLineEdit(self.centralwidget)
        self.t_user.setGeometry(QtCore.QRect(40, 190, 133, 20))
        self.t_user.setObjectName("porgreshnost_user")
        self.t_user.setValidator(QtGui.QRegExpValidator(QtCore.QRegExp("[-]{0,1}\d{0,}\.\d{0,}")))
        self.t_text = QtWidgets.QLabel(self.centralwidget)
        self.t_text.setGeometry(QtCore.QRect(40, 170, 300, 21))
        self.t_text.setObjectName("kolvo_tochek_text_2")


        # results area --------------------------------------------
        self.eiler = QtWidgets.QLabel(MainWindow)
        self.eiler.setGeometry(QtCore.QRect(200, 10, 350, 16))
        self.eiler.setObjectName("eiler")
        self.hoina = QtWidgets.QLabel(MainWindow)

        #label
        self.hoina.setGeometry(QtCore.QRect(200, 70, 150, 16))
        self.hoina.setObjectName("hoina")
        #text
        self.hoina_cifra = QtWidgets.QLabel(MainWindow)
        self.hoina_cifra.setGeometry(QtCore.QRect(200, 90, 350, 16))
        self.hoina_cifra.setObjectName("hoina_cifra")
       

        # label 
        self.koshi_text = QtWidgets.QLabel(MainWindow)
        self.koshi_text.setGeometry(QtCore.QRect(200, 150, 350, 16))
        self.koshi_text.setObjectName("koshi_text")
        # text 
        self.koshi_cifra = QtWidgets.QLabel(MainWindow)
        self.koshi_cifra.setGeometry(QtCore.QRect(200, 170, 350, 16))
        self.koshi_cifra.setObjectName("koshi_cifra")
      


        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
    def h(self):
        value_tochek1 = float(self.xs.text())
        print(value_tochek1)
        return value_tochek1
    def Tr(self):
        pgr_user1 = float(self.T.text())
        print(pgr_user1)
        return pgr_user1
    def Tao(self):
        pgr_user1 = float(self.t_user.text())
        print(pgr_user1)
        return pgr_user1
    
       
    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
       
        self.nextstep.setText(_translate("MainWindow", "посчитать"))
        self.xs_text.setText(_translate("MainWindow", "Введите h:"))
        self.T_text.setText(_translate("MainWindow", "Введите t:"))
        self.t_text.setText(_translate("MainWindow", "Введите tau:"))
        self.hoina.setText(_translate("MainWindow", "Условие Куранта"))
        self.koshi_text.setText(_translate("MainWindow", "Максимальная разность по модулю:"))

    def click_button(self,MainWindow):
        h = ui.h()
        tao = ui.Tao()
        T = ui.Tr()
        a = 1
        def f(t, x):
            return 2/(((x+t)+2)**2)
        
        def f1(t,x):
            return 1.2*(2/(((x+t)+2)**2))

        def ux(x):
            return x/(2+x)

        def dudt(x):
            return -x/((2+x)**2)

        def ut0(t):
            return 0

        def ut1(t):
            return 1/(t+3)

        razmerh = 1/h+1
        razmerT = T/tao+1
        def duch(razmerT,razmerh):

            result = np.zeros((int(razmerT),int(razmerh)))


            for i in range(0, int(razmerh)):
                result[int(razmerT)-1][i] = ux(i*h)
                if((i > 0) & (i < razmerh-1)):
                    result[int(razmerT)-2][i] = ux(i*h) + tao * dudt(i*h)


            for i in range(0, int(razmerT),  1):
                result[-i-1][0] = ut0(i*tao)
                result[-i-1][int(razmerh-1)] = ut1(i*tao)
    
            for i in range(int(razmerT)-2, 0, -1):
                for j in range(1, int(razmerh)-1):
                    result[i-1][j] = a**2 * (tao**2/h**2) * (result[i][j-1] - 2*result[i][j] + 
                    result[i][j+1]) + tao**2 * f((i*tao),(j*h)) + 2 * result[i][j] - result[i+1][j]
            return result
        def massiv2(razmerT,razmerh):
            result1 = np.zeros((int(razmerT),int(razmerh)))


            for i in range(0, int(razmerh)):
                result1[int(razmerT)-1][i] = ux(i*h)
                if((i > 0) & (i < razmerh-1)):
                    result1[int(razmerT)-2][i] = ux(i*h) + tao * dudt(i*h)


            for i in range(0, int(razmerT),  1):
                result1[-i-1][0] = ut0(i*tao)
                result1[-i-1][int(razmerh-1)] = ut1(i*tao)
    
            for i in range(int(razmerT)-2, 0, -1):
                for j in range(1, int(razmerh)-1):
                    result1[i-1][j] = a**2 * (tao**2/h**2) * (result1[i][j-1] - 2*result1[i][j] + 
                    result1[i][j+1]) + tao**2 * f1((i*tao),(j*h)) + 2 * result1[i][j] - result1[i+1][j]
            return result1  
        obch = duch(razmerT,razmerh) - massiv2(razmerT,razmerh)
        zc = Workbook()
        zc.save(filename = "result.xlsx")
        vt = zc.active

        for i in range(1, int(razmerT)+1, 1):   
            vt.cell(row = int(razmerT+2)-i, column = 1).value = 't='+str(round(((i-1)*tao),5))
    
        for i in range(1, int(razmerh)+1):   
            vt.cell(row = 1, column = i+1).value = 'x='+str(round(((i-1)*h),5)) 
    

        for i in range(int(razmerT)+1, 1, -1):
            for j in range(1, int(razmerh)+1):   
                vt.cell(row = i, column = j+1).value = duch(razmerT,razmerh)[i-2][j-1]

        zc.save("result.xlsx")
        
        zc = Workbook()
        zc.save(filename = "result1.xlsx")
        vt = zc.active

        for i in range(1, int(razmerT)+1, 1):   
            vt.cell(row = int(razmerT+2)-i, column = 1).value = 't='+str(round(((i-1)*tao),5))
    
        for i in range(1, int(razmerh)+1):   
            vt.cell(row = 1, column = i+1).value = 'x = '+str(round(((i-1)*h),5))     
    

        for i in range(int(razmerT)+1, 1, -1):
            for j in range(1, int(razmerh)+1):   
                vt.cell(row = i, column = j+1).value = massiv2(razmerT,razmerh)[i-2][j-1]

        zc.save("result1.xlsx")
        
        zc = Workbook()
        zc.save(filename = "obch.xlsx")
        vt = zc.active

        for i in range(1, int(razmerT)+1, 1):   
            vt.cell(row = int(razmerT+2)-i, column = 1).value = 't='+str(round(((i-1)*tao),5))
    
        for i in range(1, int(razmerh)+1):   
            vt.cell(row = 1, column = i+1).value = 'x='+str(round(((i-1)*h),5))        
    

        for i in range(int(razmerT)+1, 1, -1):
            for j in range(1, int(razmerh)+1):   
                vt.cell(row = i, column = j+1).value = obch[i-2][j-1]

        zc.save("obch.xlsx")
        
        def maxraxn(obch):
            return np.max(abs(obch))
        def kurant(a,tau,h):
            if(a*(tau/h) <= 1):
                return("Условие Куранта выполняется")
            else:
                return("Условие Куранта не выполняется")      
        _translate = QtCore.QCoreApplication.translate
  

        self.hoina_cifra.setText(_translate("MainWindow", (kurant(a,tao,h))))
        self.koshi_cifra.setText(_translate("MainWindow", str(maxraxn(obch))))
        
if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    app.exec()
    sys.exit(app.exec())
